# Twist 🧵

[Fault injection](https://en.wikipedia.org/wiki/Fault_injection) /w [deterministic simulation](https://www.youtube.com/watch?v=4fFDFbi3toc) for multi-threaded C++.

- [Demo](examples/wait/main.cpp)
- [Guide (ru)](docs/ru/guide.md)

## How to use

- `#include <atomic>` → `#include <twist/ed/stdlike/atomic.hpp>`
- `std::atomic<T>` → `twist::ed::stdlike::atomic<T>`

### Twist-ed `SpinLock`

```cpp
#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/wait/spin.hpp>

class SpinLock {
 public:
  void Lock() {
    // Execution backend-aware backoff for busy waiting
    twist::ed::SpinWait spin_wait;
    
    while (locked_.exchange(true)) {  // <- Faults injected here
      spin_wait();  // ~ spin_wait.SpinOnce();
    }
  }

  void Unlock() {
    locked_.store(false);  // <- Faults injected here
  }
 private:
  // (Almost) Drop-in replacement for std::atomic<T>
  twist::ed::stdlike::atomic<bool> locked_{false};
};
```

## Examples

- [Deadlock](/examples/deadlock/main.cpp)
- [Wait](/examples/wait/main.cpp)
- [SpinWait](/examples/spin/main.cpp)
- [ThreadLocal](/examples/local/main.cpp)
- [CondVar](examples/condvar/main.cpp)
- [Chrono](/examples/chrono/main.cpp)

## Standard library support

[`twist/ed/stdlike/`](/twist/ed/stdlike), namespace `twist::ed::stdlike::`
- `atomic` (`atomic.hpp`)
- `mutex` (`mutex.hpp`)
- `condition_variable` (`condition_variable.hpp`)
- `thread` (`thread.hpp`)
- `this_thread::` (`thread.hpp`)
  - `yield`
  - `sleep_for` 
  - `get_id`
- `random_device` (`random.hpp`)
- `steady_clock` + `system_clock` (`chrono.hpp`)

## Extensions

[`twist/ed/wait/`](twist/ed/wait) – waiting
- Blocking (`sys.hpp`)
  - `Wait` / `PrepareWake` + `Wake{One,All}`
- Spinning (`spin.hpp`)
  - `SpinWait`

[`twist/ed/spin/`](twist/ed/spin) – busy waiting
- `SpinLock` (`lock.hpp`)

[`twist/ed/local/`](twist/ed/local) – thread-local variables
- `ThreadLocalPtr<T>` + `TWIST_DECLARE_TL_PTR` macro (`ptr.hpp`)
- `ThreadLocal<T>` (`var.hpp`)

[`twist/ed/mutex/`](twist/ed/mutex) – mutual exclusion utilities
- `Locker<Mutex>` (`locker.hpp`)

## Layers

| Depth | Layer | Description |
| --- | --- | --- |
| 0 | [`stdlike`](twist/ed/stdlike) | _Facade_ |
| 1 | [`fault`](twist/rt/layer/fault) |  _Fault injection_ |
| 2 | [`strand`](twist/rt/layer/strand) | _Virtual threads (strands)_ |
| 3 | [`thread`](twist/rt/layer/thread) / [`fiber`](twist/rt/layer/fiber) | _Execution backend_ |

## 3 (almost) orthogonal dimensions

- Fault injection: off / on (`TWIST_FAULTY`)
- Execution backend: threads / [fibers](/twist/rt/fiber/runtime) (`TWIST_FIBERS`)
- Sanitizers: [Address](https://clang.llvm.org/docs/AddressSanitizer.html) / [Thread](https://clang.llvm.org/docs/ThreadSanitizer.html) / none (`ASAN` / `TSAN`)

![Twist](dims.png)

## Fiber execution backend

- Deterministic execution (including time and randomness)
- Fast context switches (5x as fast as context switch for OS threads)
- Randomized run queues in scheduler / wait queues in mutexes / condition variables
- Deadlock detection
- Time compression
- Compatibility with sanitizers (Address, Thread)

## CMake Options

### Runtime

- `TWIST_FAULTY` – Fault injection
- `TWIST_FIBERS` – Deterministic simulation with fibers
- `TWIST_FAST_FIBER_QUEUES` – Fast but bounded wait queues for fibers 

## Build profiles

| Name | CMake options | Description |
| --- | --- | --- |
| `FaultyFibers` |  `-DTWIST_FAULTY=ON -DTWIST_FIBERS=ON -DTWIST_PRINT_STACKS=ON` | Fault injection + Fibers execution backend |
| `FaultyThreadsASan` | `-DTWIST_FAULTY=ON -DASAN=ON` | Fault injection + OS threads + Address sanitizer |
| `FaultyThreadsTSan` | `-DTWIST_FAULTY=ON -DTSAN=ON` | Fault injection + OS threads + Thread sanitizer |

## Dependencies

`binutils-dev` package required for `-DTWIST_PRINT_STACKS=ON`

## Research

- [A Randomized Scheduler with Probabilistic Guarantees of Finding Bugs](https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/asplos277-pct.pdf)
- [A Practical Approach for Model Checking C/C++11 Code](http://plrg.eecs.uci.edu/publications/toplas16.pdf)
