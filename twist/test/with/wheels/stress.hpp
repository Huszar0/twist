#pragma once

#include <twist/rt/run/runner.hpp>

#include <wheels/test/framework.hpp>

#include <twist/test/with/wheels/env.hpp>

#include <fmt/core.h>

////////////////////////////////////////////////////////////////////////////////

#define _TWIST_TEST_IMPL(name, options) \
  void TwistTestRoutine##name();        \
  TEST(name, options) {                 \
    twist::rt::Runner runner(::twist::test::WheelsTestEnv()); \
    runner.Run(TwistTestRoutine##name); \
    runner.Finish();                    \
  }                                     \
  void TwistTestRoutine##name()

////////////////////////////////////////////////////////////////////////////////

#define TWIST_TEST(name, budget) \
  _TWIST_TEST_IMPL(name, ::wheels::test::TestOptions().TimeLimit(budget))

////////////////////////////////////////////////////////////////////////////////

#define TWIST_TEST_REPEAT(name, budget)                               \
  void TwistIteratedTestRoutine##name();                          \
  TEST(name, wheels::test::TestOptions().TimeLimit(budget)) {     \
    auto* env = twist::test::WheelsTestEnv();                     \
                                                                  \
    twist::rt::Runner runner(env);                                \
    do {                                                          \
      runner.Run(TwistIteratedTestRoutine##name);                 \
    } while (env->KeepRunning());                                 \
                                                                  \
    runner.Finish();                                              \
  }                                                               \
  void TwistIteratedTestRoutine##name()

////////////////////////////////////////////////////////////////////////////////

namespace twist::test::detail {

template <typename... Args>
class TestRunsRegistrar {
  using Self = TestRunsRegistrar;

  using TestTemplate = std::function<void(Args... args)>;
  using TL = std::chrono::milliseconds;

 public:
  TestRunsRegistrar(std::string suite, void (*routine)(Args... args))
      : suite_(suite), template_(routine) {
  }

  Self* operator->() {
    return this;
  }

  Self& Run(Args... args) {
    auto main = [temp = template_, args = std::make_tuple(args...)]() {
      std::apply(temp, args);
    };

    auto run = [main]() {
      rt::Runner runner(test::WheelsTestEnv());
      runner.Run(main);
      runner.Finish();
    };

    Add(run);

    return *this;
  }

  Self& TimeLimit(TL value) {
    time_limit_ = value;
    return *this;
  }

 private:
  void Add(std::function<void()> run) {
    std::string name = fmt::format("Run-{}", ++index_);
    auto options = wheels::test::TestOptions().TimeLimit(time_limit_);

    auto test = wheels::test::MakeTest(run, name, suite_, options);
    wheels::test::RegisterTest(std::move(test));
  }

 private:
  std::string suite_;
  TestTemplate template_;
  TL time_limit_{10s};
  size_t index_{0};
};

}  // namespace twist::test::detail

#define TWIST_TEST_TEMPLATE(suite, routine) \
  static twist::test::detail::TestRunsRegistrar UNIQUE_NAME( \
      twist_runs_registrar__) =             \
      twist::test::detail::TestRunsRegistrar(#suite, routine)
