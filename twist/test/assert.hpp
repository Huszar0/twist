#pragma once

#include <twist/rt/run/env/this.hpp>

#include <wheels/core/preprocessor.hpp>
#include <wheels/core/source_location.hpp>

#include <fmt/core.h>

#define TWIST_ASSERT(cond, error) \
  if (!(cond)) {                  \
    twist::rt::this_env->Assert(WHEELS_HERE, fmt::format("Twist assertion '{}' failed: {}", TO_STRING(cond), error)); \
  } \
