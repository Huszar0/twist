#pragma once

/*
 * Replacement for std::atomic<T>::wait/notify
 * std::atomic<T>::wait/notify api & impl are fundamentally
 * broken in many ways
 *
 * Contents
 *   namespace twist::ed
 *     fun Wait
 *     fun WaitTimed
 *     class WakeKey
 *     fun PrepareWake
 *     fun WakeOne
 *     fun WakeAll
 *
 * Usage: examples/wait/main.cpp
 *
 */

#if defined(TWIST_FAULTY)

#include <twist/rt/layer/fault/wait/sys.hpp>

namespace twist::ed {

using rt::fault::Wait;
using rt::fault::WaitTimed;

using rt::fault::WakeKey;
using rt::fault::PrepareWake;

using rt::fault::WakeOne;
using rt::fault::WakeAll;

}  // namespace twist::ed

#else

#include <twist/rt/layer/thread/wait/sys/wait.hpp>

namespace twist::ed {

using rt::thread::Wait;
using rt::thread::WaitTimed;

using rt::thread::WakeKey;
using rt::thread::PrepareWake;

using rt::thread::WakeOne;
using rt::thread::WakeAll;

}  // namespace twist::ed

#endif
