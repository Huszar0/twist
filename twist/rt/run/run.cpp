#include <twist/rt/run.hpp>

#include <twist/rt/run/env/this.hpp>
#include <twist/rt/run/env/twist.hpp>

#include <twist/rt/run/runner.hpp>

#include <fmt/core.h>

namespace twist::rt {

void Run(IEnv* env, Routine main) {
  Runner runner(env);
  runner.Run(std::move(main));
  runner.Finish();
}

void Run(Params params, Routine main) {
  TwistEnv env(params);
  Run(&env, std::move(main));
}

void Run(Routine main) {
  Run(Params(), std::move(main));
}

}  // namespace twist::rt
