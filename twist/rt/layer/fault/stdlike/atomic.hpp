#pragma once

#include <twist/rt/layer/fault/adversary/inject_fault.hpp>
#include <twist/rt/layer/fault/random/range.hpp>

#include <twist/rt/layer/strand/stdlike/atomic.hpp>

// memory_order
#include <atomic>

namespace twist::rt {
namespace fault {

// https://en.cppreference.com/w/cpp/atomic/atomic

/////////////////////////////////////////////////////////////////////

template <typename T>
class FaultyAtomic {
 public:
  using Impl = strand::stdlike::atomic<T>;

 public:
  FaultyAtomic() noexcept = default;

  FaultyAtomic(T initial_value) : impl_(initial_value) {
    AccessAdversary();
  }

  bool is_lock_free() const noexcept {
    return impl_.is_lock_free();
  }

  // NOLINTNEXTLINE
  T load(std::memory_order order = std::memory_order::seq_cst) const {
    InjectFault();
    T value = impl_.load(order);
    InjectFault();
    return value;
  }

  operator T() const noexcept {
    return load();
  }

  // NOLINTNEXTLINE
  void store(T value, std::memory_order order = std::memory_order::seq_cst) {
    InjectFault();
    impl_.store(value, order);
    InjectFault();
  }

  T operator=(T value) {
    store(value);
    return value;
  }

#define FAULTY_RMW(op)                                                       \
  T op(const T value, std::memory_order order = std::memory_order::seq_cst) { \
    InjectFault();                                                           \
    T prev_value = impl_.op(value, order);                                   \
    InjectFault();                                                           \
    return prev_value;                                                       \
  }

  FAULTY_RMW(exchange)

  // NOLINTNEXTLINE
  bool compare_exchange_weak(T& expected, T desired, std::memory_order success,
                             std::memory_order failure) {
    InjectFault();

    // Fail sometimes
    if (RandomUInteger(11) == 0) {
      return false;
    }

    bool succeeded =
        impl_.compare_exchange_weak(expected, desired, success, failure);
    InjectFault();
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order order = std::memory_order::seq_cst) {
    return compare_exchange_weak(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(T& expected, T desired,
                               std::memory_order success,
                               std::memory_order failure) {
    InjectFault();
    bool succeeded =
        impl_.compare_exchange_strong(expected, desired, success, failure);
    InjectFault();
    return succeeded;
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order order = std::memory_order::seq_cst) noexcept {
    return compare_exchange_strong(expected, desired, order, order);
  }

  // NOLINTNEXTLINE
  T fetch_add(const T value,
              std::memory_order order = std::memory_order::seq_cst) {
    InjectFault();
    T prev_value = impl_.fetch_add(value, order);
    InjectFault();
    return prev_value;
  }

  // NOLINTNEXTLINE
  T fetch_sub(const T value,
              std::memory_order order = std::memory_order::seq_cst) {
    InjectFault();
    T prev_value = impl_.fetch_sub(value, order);
    InjectFault();
    return prev_value;
  }

  // Prefix increment & decrement

  T operator++() {
    return fetch_add(1) + 1;
  }

  T operator--() {
    return fetch_sub(1) - 1;
  }

  // Postfix increment & decrement

  T operator++(int) {
    return fetch_add(1);
  }

  T operator--(int) {
    return fetch_sub(1);
  }

  // Misc

  FAULTY_RMW(fetch_and);
  FAULTY_RMW(fetch_or);
  FAULTY_RMW(fetch_xor);

#undef FAULTY_RMW

 private:
  strand::stdlike::atomic<T> impl_;
};

}  // namespace fault
}  // namespace twist::rt
