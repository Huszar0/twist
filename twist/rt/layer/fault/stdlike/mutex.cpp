#include "mutex.hpp"

#include <twist/rt/layer/fault/adversary/inject_fault.hpp>

#include <wheels/core/assert.hpp>

namespace twist::rt {
namespace fault {

FaultyMutex::FaultyMutex() : owner_id_{strand::stdlike::kInvalidThreadId} {
  AccessAdversary();
}

void FaultyMutex::lock() {  // NOLINT
  InjectFault();
  impl_.lock();
  owner_id_ = strand::stdlike::this_thread::get_id();
  InjectFault();
}

bool FaultyMutex::try_lock() {  // NOLINT
  InjectFault();
  bool acquired = impl_.try_lock();
  if (acquired) {
    owner_id_ = strand::stdlike::this_thread::get_id();
  }
  InjectFault();
  return acquired;
}

void FaultyMutex::unlock() {  // NOLINT
  InjectFault();
  WHEELS_VERIFY(owner_id_ != strand::stdlike::kInvalidThreadId,
                "Cannot unlock unlocked mutex");
  WHEELS_VERIFY(owner_id_ == strand::stdlike::this_thread::get_id(),
                "Cannot unlock mutex from another thread");
  owner_id_ = strand::stdlike::kInvalidThreadId;
  impl_.unlock();
  InjectFault();
}

}  // namespace fault
}  // namespace twist::rt
