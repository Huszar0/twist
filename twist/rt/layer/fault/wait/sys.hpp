#pragma once

#include <twist/rt/layer/fault/stdlike/atomic.hpp>

#include <twist/rt/layer/strand/wait/sys.hpp>

#include <cstdint>

namespace twist::rt::fault {

void Wait(FaultyAtomic<uint32_t>& atomic, uint32_t old);
bool WaitTimed(FaultyAtomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout);

using strand::WakeKey;
WakeKey PrepareWake(FaultyAtomic<uint32_t>& atomic);

void WakeOne(WakeKey key);
void WakeAll(WakeKey key);

}  // namespace twist::rt::fault
