#pragma once

#if defined(TWIST_FIBERS)

// cooperative user-space fibers

#include <twist/rt/layer/fiber/stdlike/atomic.hpp>

namespace twist::rt::strand::stdlike {

template <typename T>
using atomic = fiber::Atomic<T>;

}  // namespace twist::rt::strand::stdlike

#else

// native threads

#include <atomic>

namespace twist::rt::strand::stdlike {

using ::std::atomic;

}  // namespace twist::rt::strand::stdlike

#endif
