#pragma once

#include <wheels/core/assert.hpp>

#include <array>
#include <atomic>
#include <functional>

namespace twist::rt::strand {

//////////////////////////////////////////////////////////////////////

extern const uintptr_t kSlotUninitialized;

class Slot {
 public:
  void Store(void* data) {
    ptr_ = reinterpret_cast<uintptr_t>(data);
  }

  template <typename T>
  T* LoadAs() const {
    return reinterpret_cast<T*>(ptr_);
  }

  bool IsInit() const {
    return ptr_ != kSlotUninitialized;
  }

 private:
  uintptr_t ptr_ = kSlotUninitialized;
};

//////////////////////////////////////////////////////////////////////

static const size_t kTLSSlots = 1024;

using TLS = std::array<Slot, kTLSSlots>;

//////////////////////////////////////////////////////////////////////

// Singleton
// Access via TLSManager::Instance()

class TLSManager {
 public:
  using Ctor = std::function<void*()>;
  using Dtor = std::function<void(void*)>;

  struct Var {
    Ctor ctor;
    Dtor dtor;
  };

 public:
  static TLSManager& Instance() {
    static TLSManager instance;
    return instance;
  }

  size_t AcquireSlot(Var var) {
    size_t index = next_free_slot_.fetch_add(1);
    WHEELS_VERIFY(index < kTLSSlots, "TLS over-commit");
    vars_[index] = std::move(var);
    return index;
  }

  Slot* Access(size_t slot_index) {
    return Access(slot_index, AccessTLS());
  }

  Slot* Access(size_t slot_index, TLS& tls) {
    if (tls[slot_index].IsInit()) {
      return &tls[slot_index];
    }

    // Initialize
    tls[slot_index].Store(vars_[slot_index].ctor());

    return &tls[slot_index];
  }

  void Destroy(TLS& tls) {
    size_t slots_used = next_free_slot_.load();

    for (size_t i = 0; i < slots_used; ++i) {
      if (tls[i].IsInit()) {
        vars_[i].dtor(tls[i].LoadAs<void>());
      }
    }
  }

 private:
  static TLS& AccessTLS();

 private:
  Var vars_[kTLSSlots];
  std::atomic<size_t> next_free_slot_{0};
};

//////////////////////////////////////////////////////////////////////

class ManagedTLS {
 public:
  ManagedTLS() {
    tls_.fill({});
  }

  TLS& Access() {
    return tls_;
  }

  ~ManagedTLS() {
    TLSManager::Instance().Destroy(tls_);
  }

 private:
  TLS tls_;
};

}  // namespace twist::rt::strand
