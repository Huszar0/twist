#pragma once

#include <cstdint>

namespace twist::rt::thread {

uint64_t GenerateRandomUInt64();

}  // namespace twist::rt::thread
