#pragma once

#include <twist/rt/layer/thread/wait/sys/wake_key.hpp>

#include <atomic>
#include <chrono>

namespace twist::rt::thread {

// Wait

void Wait(std::atomic<uint32_t>& atomic, uint32_t old);
bool WaitTimed(std::atomic<uint32_t>& atomic, uint32_t old, std::chrono::milliseconds timeout);

// Wake

WakeKey PrepareWake(std::atomic<uint32_t>& atomic);

void WakeOne(WakeKey token);
void WakeAll(WakeKey key);

}  // namespace twist::rt::thread
