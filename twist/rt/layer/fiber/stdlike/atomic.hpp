#pragma once

#include <wheels/core/compiler.hpp>

// std::memory_order
#include <atomic>
// std::exchange
#include <utility>

namespace twist::rt::fiber {

template <typename T>
class Atomic {
 public:
  Atomic() noexcept = default;

  Atomic(T initial_value) : value_(initial_value) {
  }

  // Non-copyable
  Atomic(const Atomic<T>&) = delete;
  Atomic<T>& operator=(const Atomic<T>&) = delete;

  // Non-movable
  Atomic(Atomic<T>&&) = delete;
  Atomic<T>& operator=(Atomic<T>&&) = delete;

  bool is_lock_free() const noexcept {
    return true;
  }

  // NOLINTNEXTLINE
  T load(std::memory_order mo = std::memory_order::seq_cst) const {
    WHEELS_UNUSED(mo);
    return value_;
  }

  operator T() const {
    return value_;
  }

  // NOLINTNEXTLINE
  void store(T new_value, std::memory_order mo = std::memory_order::seq_cst) {
    WHEELS_UNUSED(mo);
    value_ = new_value;
  }

  T operator=(T new_value) {
    value_ = new_value;
    return new_value;
  }

  // NOLINTNEXTLINE
  T exchange(T new_value, std::memory_order mo = std::memory_order::seq_cst) {
    WHEELS_UNUSED(mo);
    return RMW([new_value](T /*curr_value*/) {
      return new_value;
    });
  }

  // NOLINTNEXTLINE
  bool compare_exchange_strong(
      T& expected, T desired,
      std::memory_order success = std::memory_order::seq_cst,
      std::memory_order failure = std::memory_order::seq_cst) {
    WHEELS_UNUSED(success);
    WHEELS_UNUSED(failure);

    if (value_ == expected) {
      value_ = desired;
      return true;
    } else {
      expected = value_;
      return false;
    }
  }

  // NOLINTNEXTLINE
  bool compare_exchange_weak(
      T& expected, T desired,
      std::memory_order success = std::memory_order::seq_cst,
      std::memory_order failure = std::memory_order::seq_cst) {
    return compare_exchange_strong(expected, desired, success, failure);
  }

  // NOLINTNEXTLINE
  T fetch_add(T delta, std::memory_order mo = std::memory_order::seq_cst) {
    static_assert(std::is_integral_v<T>, "Integral type required");
    WHEELS_UNUSED(mo);
    return RMW([delta](T curr_value) {
      return curr_value + delta;
    });
  }

  // NOLINTNEXTLINE
  T fetch_sub(T delta, std::memory_order mo = std::memory_order::seq_cst) {
    static_assert(std::is_integral_v<T>, "Integral type required");
    WHEELS_UNUSED(mo);
    return RMW([delta](T curr_value) {
      return curr_value - delta;
    });
  }

  // NOLINTNEXTLINE
  T fetch_or(T or_value, std::memory_order mo = std::memory_order::seq_cst) {
    WHEELS_UNUSED(mo);
    return RMW([or_value](T curr_value) {
      return curr_value | or_value;
    });
  }

  // NOLINTNEXTLINE
  T fetch_and(T and_value, std::memory_order mo = std::memory_order::seq_cst) {
    WHEELS_UNUSED(mo);
    return RMW([and_value](T curr_value) {
      return curr_value & and_value;
    });
  }

  // NOLINTNEXTLINE
  T fetch_xor(T xor_value, std::memory_order mo = std::memory_order::seq_cst) {
    WHEELS_UNUSED(mo);
    return RMW([xor_value](T curr_value) {
      return curr_value ^ xor_value;
    });
  }

  // wait / notify

  // Intentionally missing
  // Use twist::ed::Wait/Wake{One,All}

 private:
  template <typename F>
  T RMW(F modify) {
    T old_value = value_;             // Read
    T new_value = modify(old_value);  // Modify
    value_ = new_value;               // Write
    return old_value;
  }

 private:
  T value_;
};

}  // namespace twist::rt::fiber
