#pragma once

#include <twist/rt/layer/fiber/runtime/fwd.hpp>
#include <twist/rt/layer/fiber/runtime/routine.hpp>
#include <twist/rt/layer/fiber/runtime/watcher.hpp>
#include <twist/rt/layer/fiber/runtime/wait_queue.hpp>

namespace twist::rt::fiber {

class ThreadLike : private IFiberWatcher {
  // Attached [ -> Completed ] -> Detached
  enum class State {
    Attached,
    Completed,
    Detached,
  };

 public:
  ThreadLike(FiberRoutine routine);
  ~ThreadLike();

  // Non-copyable
  ThreadLike(const ThreadLike&) = delete;
  ThreadLike& operator=(const ThreadLike&) = delete;

  // Movable
  ThreadLike(ThreadLike&&);
  ThreadLike& operator =(ThreadLike&&);

  bool joinable() const;  // NOLINT
  void join();  // NOLINT
  void detach();  // NOLINT

 private:
  void Reset();
  void MoveFrom(ThreadLike& that);

  // IFiberWatcher
  void Completed() noexcept override;

 private:
  Fiber* fiber_;
  State state_;
  WaitQueue join_{"thread::join"};
};

}  // namespace twist::rt::fiber
