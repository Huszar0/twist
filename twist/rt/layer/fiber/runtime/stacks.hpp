#pragma once

#include <sure/stack.hpp>

#include <wheels/core/size_literals.hpp>

#include <vector>

using namespace wheels::size_literals;

namespace twist::rt::fiber {

sure::Stack AllocateStack();

void ReleaseStack(sure::Stack stack);

void SetMinStackSize(size_t bytes);

}  // namespace twist::rt::fiber
