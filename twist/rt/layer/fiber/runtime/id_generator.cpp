#include <twist/rt/layer/fiber/runtime/id_generator.hpp>

namespace twist::rt::fiber {

const FiberId kInvalidFiberId = 0;

}  // namespace twist::rt::fiber
