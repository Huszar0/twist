#include <twist/rt/layer/fiber/runtime/stacks.hpp>

#include <vector>

namespace twist::rt::fiber {

using sure::Stack;

//////////////////////////////////////////////////////////////////////

class StackAllocator {
  using Stack = sure::Stack;

 public:
  StackAllocator();

  Stack Allocate();
  void Release(Stack stack);

  void SetMinStackSize(size_t bytes);

 private:
  static size_t DefaultStackSize() {
    return 64_KiB;
  }

 private:
  std::vector<Stack> pool_;
  size_t stack_size_;
};

StackAllocator::StackAllocator()
    : stack_size_(DefaultStackSize()) {
}

Stack StackAllocator::Allocate() {
  if (!pool_.empty()) {
    Stack stack = std::move(pool_.back());
    pool_.pop_back();
    return stack;
  }
  return Stack::AllocateBytes(/*at_least=*/stack_size_);
}

void StackAllocator::Release(Stack stack) {
  pool_.push_back(std::move(stack));
}

void StackAllocator::SetMinStackSize(size_t bytes) {
  stack_size_ = bytes;
}

//////////////////////////////////////////////////////////////////////

static thread_local StackAllocator allocator;

Stack AllocateStack() {
  return allocator.Allocate();
}

void ReleaseStack(Stack stack) {
  allocator.Release(std::move(stack));
}

void SetMinStackSize(size_t bytes) {
  allocator.SetMinStackSize(bytes);
}

}  // namespace twist::rt::fiber
