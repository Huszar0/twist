#pragma once

#include <cstdlib>

namespace twist::rt::fiber {

using FiberId = size_t;

extern const FiberId kInvalidFiberId;

}  // namespace twist::rt::fiber
