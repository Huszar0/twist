#pragma once

namespace twist::rt::fiber {

enum class FiberState {
  Starting,  // Initial state
  Runnable,  // In Scheduler run queue
  Running,
  Suspended,  // In wait queue or Scheduler sleep queue
  Terminated,
  Deadlocked,
};

}  // namespace twist::rt::fiber
