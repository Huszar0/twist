#include <twist/strand/thread_local.hpp>

#include <wheels/test/test_framework.hpp>

#include <twist/fiber/runtime/api.hpp>

#include <iostream>
#include <vector>
#include <unordered_set>
#include <thread>

using twist::strand::ThreadLocal;

class TestObject {
 public:
  TestObject(std::string name) : name_(std::move(name)) {
    std::cout << "TestObject '" << name_ << "' created" << std::endl;
  }

  ~TestObject() {
    std::cout << "TestObject '" << name_ << "' destroyed" << std::endl;
  }

  const std::string& Name() const {
    return name_;
  }

  void SetName(std::string name) {
    name_ = name;
  }

 private:
  std::string name_;
};

TEST_SUITE(ThreadLocal) {
  SIMPLE_TEST(Threads) {
    ThreadLocal<TestObject> tl(TestObject("Joe"));

    std::thread t([&tl]() {
      tl->SetName("Homer");
      ASSERT_EQ(tl->Name(), "Homer");
    });
    t.join();

    ASSERT_EQ(tl->Name(), "Joe");
  }

  SIMPLE_TEST(Fibers) {
    twist::fiber::RunScheduler([]() {
      ThreadLocal<TestObject> tl(TestObject("Joe"));

      twist::fiber::Spawn([&tl]() {
        tl->SetName("Homer");
        ASSERT_EQ(tl->Name(), "Homer");
      });

      twist::fiber::Yield();

      ASSERT_EQ(tl->Name(), "Joe");
    });
  }
}