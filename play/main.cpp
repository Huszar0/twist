#include <twist/ed/stdlike/atomic.hpp>

#include <twist/rt/run.hpp>

#include <fmt/core.h>

int main() {
  twist::rt::Run([]() {
    twist::ed::stdlike::atomic<bool> fun{false};
    // Your code goes here
  });

  return 0;
}
